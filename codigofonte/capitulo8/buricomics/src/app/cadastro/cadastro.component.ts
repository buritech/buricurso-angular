// Arquivo: src/app/cadastro/cadastro.component.ts
import { Component, OnInit, Input } from '@angular/core';
import { Gibi } from '../model';
import { GibiService } from '../servicos/gibi.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
    selector: 'cadastro',
    templateUrl: './cadastro.component.html',
    styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
    gibi: Gibi = new Gibi()
    mensagem: string = ''
    formCadastro: FormGroup
// Arquivo: src/app/cadastro/cadastro.component.ts
// Código omitido acima
    constructor(private servico: GibiService,
        private formBuilder: FormBuilder) { }
        private criarForm() {
            this.formCadastro = this.formBuilder.group({
                titulo: ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(5)
                ])],
                preco: ['', Validators.required],
                editora: ['', Validators.required],
                url: ['', Validators.required],
                semValidacao: [''],
            })
        }
// Código omitido abaixo
        ngOnInit() {
            this.criarForm()
        }
    salvar() {
        console.log(this.gibi)
        this.servico.salvar(this.gibi).subscribe(
            resultado => {
                this.mensagem = `Gibi salvo: [${this.gibi.titulo}]`
                console.log(this.mensagem)
                this.gibi = new Gibi()
                this.criarForm()
            },
            error => { console.log(error) }
        )
    }
}

