import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutModule } from '../template/layout.module';
import { CadastroComponent } from './cadastro.component';
import { GibiModule } from '../gibi/gibi.module';
import { routing } from '../app.routes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Arquivo: src/app/cadastro/cadastro.module.ts
// Código omitido acima
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [ CommonModule, LayoutModule, GibiModule, 
    FormsModule, NgbModule.forRoot(), routing, ReactiveFormsModule],
  declarations: [CadastroComponent],
  exports: [CadastroComponent]
})
export class CadastroModule { }
