import { Component, OnInit } from '@angular/core';
import { Gibi, ExclusaoListener } from '../model';
import { GibiService } from '../servicos/gibi.service';

@Component({
    selector: 'gibizada',
    templateUrl: './gibilista.component.html',
    styleUrls: ['./gibilista.component.css']
})
//Arquivo: src/app/gibilista/gibilista.component.ts
//Código omitido acima
export class GibiListaComponent implements OnInit, ExclusaoListener {
    private gibizada: Gibi[] = []
    private _mensagem: string = ''
    private _exclusaoListener: ExclusaoListener

    exclusaoSucesso(gibi: Gibi) {
        this._mensagem = `Gibi ${gibi.titulo} excluída`
        console.log(this._mensagem)
        this.gibizada = this.gibizada
            .filter(filterValue => filterValue != gibi)
        setTimeout(() => { this._mensagem = '' }, 5000)
    }
    ngOnInit() {
        this._exclusaoListener = this
    }
    constructor(private _servico: GibiService) {
        this._servico.listar()
            .subscribe(
                gibis => this.gibizada = gibis,
                erro => console.log(erro)
            )
    }

}
