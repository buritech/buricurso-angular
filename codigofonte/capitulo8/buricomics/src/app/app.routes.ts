  // Arquivo: src/app/app.routes.ts
  import { RouterModule, Routes } from '@angular/router';
  import { HomeComponent } from './home/home.component';
  import { CadastroComponent } from './cadastro/cadastro.component';

  const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'cadastro', component: CadastroComponent },
    { path: '**', redirectTo: '' }
  ];

  export const routing = RouterModule.forRoot(appRoutes)
