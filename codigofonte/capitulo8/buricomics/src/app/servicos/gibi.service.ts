// Arquivo: src/app/servico/gibi.service.ts
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Gibi } from '../model';

// Arquivo: src/app/servico/gibi.service.ts  
//Código omitido acima
@Injectable()
export class GibiService {
    private  _url: string = "http://localhost:3000/bc"
    private _httpOptions = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'Authorization': 'my-auth-token'
        })
    }
    constructor(private _httpClient: HttpClient) { }
    excluir(gibi:Gibi): Observable<Response> { 
        return this._httpClient
            .delete<Response>(`${this._url}/gibis/${gibi._id}`, 
                                this._httpOptions);
    }
    //Código omitido abaixo
    salvar(gibi:Gibi): Observable<Gibi> {
        return this._httpClient
            .post<Gibi>(this._url + "/gibis", gibi, this._httpOptions);
    }

    listar(): Observable<Gibi[]> {
        return this._httpClient
            .get<Gibi[]>(this._url + "/gibis");
    }

}