  // Arquivo: src/app/gibi/gibi.component.ts

  import { Component, OnInit, Input } from '@angular/core';
  import { Gibi, ExclusaoListener } from '../model';
  import { GibiService } from '../servicos/gibi.service';
  import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

  //Código omitido abaixo

@Component({
  selector: 'gibi',
  templateUrl: './gibi.component.html',
  styleUrls: ['./gibi.component.css']
})
export class GibiComponent implements OnInit {
  @Input() gibi: Gibi
  @Input() exibirDetalhes: boolean
  @Input() margem: string
  @Input() listener: ExclusaoListener
  
  excluir(gibi: Gibi) {
    this._servico
    .excluir(gibi)
    .subscribe(
      () => { this.listener.exclusaoSucesso(gibi) },
      erro => console.log(erro)
    )
  }
  
  ngOnInit() { }
  // Arquivo: src/app/gibi/gibi.component.ts
  //Código omitido acima
  constructor(private _servico: GibiService, 
              private _modalService: NgbModal) {
    this.exibirDetalhes = true
    this.margem = "mb-4"
  }
  open(content) {
    this._modalService.open(content).result.then(
      (result) => {
        if(result=='confirmar') 
          this.excluir(this.gibi)
        }, 
      (reason) => {
        console.log(`Dismissed ${this.getDismissReason(reason)}`);
      }
    );
  }//Código omitido abaixo

  // Arquivo: src/app/gibi/gibi.component.ts
  // Código omitido acima
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
