import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from "angular-font-awesome";

import { GibiComponent } from './gibi.component';
import { GibiListaComponent } from '../gibilista/gibilista.component';
import { routing } from '../app.routes';
// Arquivo: src/app/gibi/gibi.module.ts
//Código omitido acima
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, NgbModule.forRoot(), 
      AngularFontAwesomeModule, routing, ],
  declarations: [GibiComponent, GibiListaComponent],
  exports: [GibiComponent, GibiListaComponent],
  providers: []
})
export class GibiModule { }
