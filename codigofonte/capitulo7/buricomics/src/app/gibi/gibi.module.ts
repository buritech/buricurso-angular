  // Arquivo: src/app/gibi/gibi.module.ts
  import { NgModule } from '@angular/core';
  import { CommonModule } from '@angular/common';
  import { AngularFontAwesomeModule } from "angular-font-awesome";

  import { GibiComponent } from './gibi.component';
  import { GibiListaComponent } from '../gibilista/gibilista.component';
  import { routing } from '../app.routes';

  @NgModule({
    imports: [ CommonModule, AngularFontAwesomeModule, routing],
    declarations: [GibiComponent, GibiListaComponent],
    exports:      [GibiComponent, GibiListaComponent],
    providers:[]
  })
  export class GibiModule { }
