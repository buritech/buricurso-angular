import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from "angular-font-awesome";

import { GibiComponent } from './gibi.component';
import { GibiListaComponent } from '../gibilista/gibilista.component';

@NgModule({
  imports: [ CommonModule, AngularFontAwesomeModule ],
  declarations: [GibiComponent, GibiListaComponent],
  exports:      [GibiComponent, GibiListaComponent]
})
export class GibiModule { }
