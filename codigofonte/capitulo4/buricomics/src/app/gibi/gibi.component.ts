import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'gibi',
  templateUrl: './gibi.component.html',
  styleUrls: ['./gibi.component.css']
})
export class GibiComponent implements OnInit {
  @Input() titulo:string
  @Input() url:string
  @Input() resumo:string
  @Input() preco:number
  constructor() { }
  ngOnInit() { }
}
