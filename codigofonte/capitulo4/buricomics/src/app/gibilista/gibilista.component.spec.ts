import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GibilistaComponent } from './gibilista.component';

describe('GibilistaComponent', () => {
  let component: GibilistaComponent;
  let fixture: ComponentFixture<GibilistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GibilistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GibilistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
