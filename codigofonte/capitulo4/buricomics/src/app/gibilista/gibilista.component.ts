import { Component, OnInit } from '@angular/core';
import { GibiComponent } from '../gibi/gibi.component';

@Component({
  selector: 'gibizada',
  templateUrl: './gibilista.component.html',
  styleUrls: ['./gibilista.component.css']
})
export class GibiListaComponent implements OnInit {
  gibizada:GibiComponent[] = []
  constructor() { 
    let gibi = new GibiComponent()
    gibi.titulo='Os vingadores'
    gibi.url='assets/img/vingadores.jpg'
    gibi.resumo=`Os Vingadores são um grupo de super-heróis 
    de história em quadrinhos 
    publicados nos Estados Unidos pela editora Marvel Comics.`
    gibi.preco=144.50
    this.gibizada.push(gibi)
    //continua..
    gibi = new GibiComponent()
    gibi.titulo='Lobo'
    gibi.url='assets/img/lobo.jpg'
    gibi.resumo=`Lobo é um personagem de histórias em quadrinhos 
    (banda desenhada) da DC Comics, criado em 1983 por Keith Giffen 
    e Roger Slifer.`
    gibi.preco=221.87
    this.gibizada.push(gibi)

    gibi = new GibiComponent()
    gibi.titulo='A piada mortal'
    gibi.url='assets/img/batman-a-piada-mortal.jpg'
    gibi.resumo=`Batman: The Killing Joke (Batman: A Piada Mortal) 
    é um romance gráfico de one-shot escrito pelo autor Alan Moore e 
    desenhado por Brian Bolland.`
    gibi.preco=350.19
    this.gibizada.push(gibi)
  }
  ngOnInit() {}
}
