import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LayoutModule } from './template/layout.module';
import { CarrosselModule } from './carrossel/carrossel.module';
import { BoasVindasModule } from './boasvindas/boasvindas.module';
import { GibiModule } from './gibi/gibi.module';

@NgModule({
  declarations: [ AppComponent ],
  imports: [ BrowserModule, LayoutModule, 
    CarrosselModule, BoasVindasModule, GibiModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
