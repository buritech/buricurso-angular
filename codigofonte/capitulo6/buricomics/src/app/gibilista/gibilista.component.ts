import { Component, OnInit } from '@angular/core';
import { Gibi } from '../model';
import { GibiService } from '../servicos/gibi.service';

@Component({
    selector: 'gibizada',
    templateUrl: './gibilista.component.html',
    styleUrls: ['./gibilista.component.css']
})
export class GibiListaComponent implements OnInit {
    private gibizada: Gibi[] = []
    constructor(private _servico: GibiService) {
        this._servico.listar()
            .subscribe(
                gibis => this.gibizada = gibis,
                erro => console.log(erro)
            )
    }
    ngOnInit() { }
}
