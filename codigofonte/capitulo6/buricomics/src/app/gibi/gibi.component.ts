// Arquivo: src/app/gibi/gibi.component.ts
import { Component, OnInit, Input } from '@angular/core';
import { Gibi } from '../model';
@Component({
  selector: 'gibi',
  templateUrl: './gibi.component.html',
  styleUrls: ['./gibi.component.css']
})
export class GibiComponent implements OnInit {
  @Input() gibi: Gibi
  @Input() exibirDetalhes: boolean
  @Input() margem: string
  constructor() {
    this.exibirDetalhes = true
    this.margem = "mb-4"
    this.gibi = new Gibi()
    this.gibi.titulo = 'Os vingadores'
    this.gibi.resumo = 'Os Vingadores são ...'
    this.gibi.preco = 144.5
    this.gibi.url = 'assets/img/vingadores.jpg'
  }
  ngOnInit() { }
}
