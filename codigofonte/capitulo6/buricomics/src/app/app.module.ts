import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LayoutModule } from './template/layout.module';
import { CarrosselModule } from './carrossel/carrossel.module';
import { BoasVindasModule } from './boasvindas/boasvindas.module';
import { GibiModule } from './gibi/gibi.module';
import { GibiService } from './servicos/gibi.service';
import { HttpClientModule } from '@angular/common/http';
import { CadastroModule } from './cadastro/cadastro.module';
// Arquivo: src/app/app.module.ts
// Código omitido acima
import { HomeComponent } from './home/home.component';
import { routing } from './app.routes';

@NgModule({
    declarations: [AppComponent, HomeComponent],
    imports: [BrowserModule, LayoutModule, CarrosselModule,
        BoasVindasModule, GibiModule, HttpClientModule, 
        CadastroModule, routing],
    providers: [GibiService],
    bootstrap: [AppComponent]
})
export class AppModule { }