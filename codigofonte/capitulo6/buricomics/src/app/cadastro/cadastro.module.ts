// Arquivo: src/app/cadastro/cadastro.module.ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroComponent } from './cadastro.component';
import { LayoutModule } from '../template/layout.module';
import { GibiModule } from '../gibi/gibi.module';

@NgModule({
  imports: [
    CommonModule, LayoutModule, GibiModule
  ],
  declarations: [CadastroComponent],
  exports: [CadastroComponent]
})
export class CadastroModule { }
