import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CabecalhoComponent } from './cabecalho.component';
import { RodapeComponent } from '../rodape/rodape.component';

@NgModule({
        imports:[NgbModule.forRoot()], 
        declarations: [CabecalhoComponent, RodapeComponent],
        exports: [CabecalhoComponent, RodapeComponent]
})
export class LayoutModule { }