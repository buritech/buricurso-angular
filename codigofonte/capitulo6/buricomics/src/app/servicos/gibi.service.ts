import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { Gibi } from '../model';

@Injectable()
export class GibiService {

    private  _url: string = "http://localhost:3000/bc"

    constructor(private _httpClient: HttpClient) { }
    
    listar(): Observable<Gibi[]> {
        return this._httpClient
            .get<Gibi[]>(this._url + "/gibis");
    }
}