var db = require('../../config/database');

var api = {}

api.adiciona = function (req, res) {
    var gibi = req.body;
    delete gibi._id;
    db.insert(gibi, function (err, newDoc) {
        if (err)
            return console.log(err);
        console.log('Gibi cadastrado com sucesso: ' + newDoc._id);
        res.json(newDoc._id);
    });
};

api.busca = function (req, res) {
    db.findOne({ _id: req.params.gibiId }, function (err, doc) {
        if (err)
            return console.log(err);
        console.log('Gibi encontrado: ' + doc._id);
        res.json(doc);
    });
};

api.atualiza = function (req, res) {
    console.log('Parâmetro recebido:' + req.params.gibiId);
    db.update({ _id: req.params.gibiId }, req.body, function (err, numReplaced) {
        if (err) return console.log(err);
        if (numReplaced) res.status(200).end();
        res.status(500).end();
        console.log('Gibi atualizado com sucesso: ' + req.body._id);
        res.status(200).end();
    });
};

api.lista = function (req, res) {
    db.find({}).sort({ titulo: 1 }).exec(function (err, doc) {
        if (err)
            return console.log(err);
        console.log('GET - /bc/gibis')
        res.json(doc);
    });
};

api.listaPorEditora = function (req, res) {
    var editoraId = parseInt(req.params.editoraId);
    db.find({ editora: editoraId }, function (err, doc) {
        if (err)
            return console.log(err);
        res.json(doc);
    });

};

api.remove = function (req, res) {

    db.remove({ _id: req.params.gibiId }, {}, function (err, numRemoved) {
        if (err)
            return console.log(err);
        console.log('Gibi excluído com sucesso ' + req.params.gibiId);
        if (numRemoved)
            res.status(200).end();
        res.status(500).end();
    });
};

api.listaEditoras = function (req, res) {

    res.json([
        {
            _id: 1,
            nome: 'Marvel'
        },
        {
            _id: 2,
            nome: 'DC',
        },
        {
            _id: 3,
            nome: 'Globo'
        }
    ]);

};


module.exports = api;