// Arquivo: src/app/servico/gibi.service.ts
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Gibi } from '../model';

@Injectable()
export class GibiService {
  private _url: string = "http://localhost:3000/bc"
  private _httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
    })
  }
  // após a declaração de atributos...
  constructor(private _httpClient: HttpClient) { }
  consultar(id: string): Observable<Gibi> {
    return this._httpClient
      .get<Gibi>(`${this._url}/gibis/${id}`);
  }

  excluir(gibi: Gibi): Observable<Response> {
    return this._httpClient
      .delete<Response>(`${this._url}/gibis/${gibi._id}`,
        this._httpOptions);
  }

  // Arquivo: src/app/servico/gibi.service.ts  
  //Código omitido acima
  salvar(gibi: Gibi): Observable<Gibi> {
    if (gibi._id)
      return this._httpClient
        .put<Gibi>(`${this._url}/gibis/${gibi._id}`, 
                            gibi, this._httpOptions);
    else
      return this._httpClient
        .post<Gibi>(this._url + "/gibis", gibi, 
                                  this._httpOptions);
  }
  //Código omitido abaixo

  listar(): Observable<Gibi[]> {
    return this._httpClient
      .get<Gibi[]>(this._url + "/gibis");
  }
}