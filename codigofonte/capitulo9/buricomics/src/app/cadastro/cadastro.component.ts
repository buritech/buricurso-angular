// Arquivo: src/app/cadastro/cadastro.component.ts
import { Component, OnInit, Input } from '@angular/core';
import { Gibi } from '../model';
import { GibiService } from '../servicos/gibi.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
  gibi: Gibi = new Gibi()
  mensagem: string = ''
  formCadastro: FormGroup
  constructor(private servico: GibiService,
    private formBuilder: FormBuilder,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
  ) { }
  ngOnInit() {
    this.criarForm()
    this.carregarGibi()
  }
  
  private carregarGibi() {
    this._activatedRoute.params.subscribe(
      parametros => {
        if (parametros.id)
        this.servico.consultar(parametros.id)
        .subscribe(
          gibi => this.gibi = gibi,
          erro => console.log(erro)
        )
      }
    )
  }
  private criarForm() {
    this.formCadastro = this.formBuilder.group({
      titulo: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])],
      preco: ['', Validators.required],
      editora: ['', Validators.required],
      url: ['', Validators.required],
      semValidacao: [''],
    })
  }
  
  // Arquivo: src/app/cadastro/cadastro.component.ts
  // Código omitido acima
  salvar() {
    this.servico.salvar(this.gibi).subscribe(
      resultado => {
        this.mensagem = `Gibi salvo: [${this.gibi.titulo}]`
        if (this.gibi._id) {
          
        } else {
          this.gibi = new Gibi()
          this.criarForm()
        }
      },
      error => { console.log(error) }
    )
  }
}

