// Arquivo: src/app/model.ts
export class Gibi {
    _id:string
    titulo: string
    url: string
    resumo: string
    preco: number
    constructor(){
        this.url = "assets/img/no_image.jpg"
    }
}

export interface ExclusaoListener{
    exclusaoSucesso?(gibi:Gibi): void;
}
