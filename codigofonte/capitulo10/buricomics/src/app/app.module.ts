import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { AppComponent } from './app.component';
import { LayoutModule } from './template/layout.module';
import { CarrosselModule } from './carrossel/carrossel.module';
import { BoasVindasModule } from './boasvindas/boasvindas.module';
import { GibiModule } from './gibi/gibi.module';
import { GibiService } from './servicos/gibi.service';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CadastroModule } from './cadastro/cadastro.module';
import { HomeComponent } from './home/home.component';
import { routing } from './app.routes';
import { LoginModule } from './login/login.module';
import { LoginService } from './servicos/login.service';
/*
import { AuthGuardService } from './auth/auth-guard.service';
import { TokenInterceptor } from './auth/auth.interceptor';
*/
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { TokenInterceptor } from './auth/auth.interceptor';
//Arquivo: src/app/app.module.ts
//Código acima omitido
import { AuthGuardService } from './auth/auth-guard.service';
@NgModule({
    declarations: [AppComponent, HomeComponent],
    imports: [BrowserModule, LayoutModule, CarrosselModule,
        BoasVindasModule, GibiModule, HttpClientModule,
        CadastroModule, routing, LoginModule, AuthModule
    ],
    providers: [GibiService, LoginService, AuthService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }, AuthGuardService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }