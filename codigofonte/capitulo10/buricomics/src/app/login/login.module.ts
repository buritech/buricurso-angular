import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
//Arquivo: src/app/login/login.module.ts
//Código acima omitido
import { LayoutModule } from '../template/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { routing } from '../app.routes';

@NgModule({
  imports: [
    CommonModule, LayoutModule, 
    FormsModule, NgbModule.forRoot(), routing, ReactiveFormsModule
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent]
})
export class LoginModule { }
