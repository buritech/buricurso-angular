//Arquivo: src/app/login/login.component.ts
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../servicos/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private _usuario: Usuario = new Usuario()
  private _formLogin: FormGroup
  private _mensagem: string
  private _returnUrl: string

  constructor(private _servico: LoginService,
    private formBuilder: FormBuilder,
    private _router: Router) {
    localStorage.removeItem('usuarioLogado')
  }

  private criarForm() {
    this._formLogin = this.formBuilder.group({
      login: ['', Validators.required],
      senha: ['', Validators.required],
    })
  }

  ngOnInit() {
    this.criarForm()
    this._returnUrl = '/'
  }
  //Arquivo: src/app/login/login.component.ts
  //Código acima omitido    
  efetuarLogin() {
    this._servico.autenticar(this._usuario).subscribe(
      usuario => {
        if (usuario) {
          this._mensagem = ""
          this._usuario = usuario
          localStorage.setItem('usuarioLogado',
            JSON.stringify(this._usuario));
          console.log('[LoginComponent] token=='+this._usuario.token)
          this._router.navigate([this._returnUrl]);
        } else {
          this.criarForm()
          this._mensagem = 'Login/Senha inválido(s)'
        }
      },
      error => {
        this._mensagem = 'Login/Senha inválido(s)'
        console.log(error)
      }
    )
  }
}
