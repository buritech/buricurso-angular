//Arquivo: src/app/template/cabecalho/cabecalho.component.ts
import { Component, Output, EventEmitter } from '@angular/core';
@Component({
	selector: 'cabecalho',
	templateUrl: './cabecalho.component.html',
	styleUrls: ['./cabecalho.component.css']
})
export class CabecalhoComponent {
	titulo = 'buricomics'
	@Output() alterarConsulta = new EventEmitter<string>();

	onAlterarConsulta(consulta: string) {
		this.alterarConsulta.emit(consulta+" ");
	}
}