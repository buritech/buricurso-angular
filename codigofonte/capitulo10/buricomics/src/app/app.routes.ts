// Arquivo: src/app/app.routes.ts
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth/auth-guard.service';

// Arquivo: src/app/app.routes.ts
// Código acima omitido
const appRoutes: Routes = [
  { path: '', component: HomeComponent, 
              canActivate: [AuthGuardService]},
  { path: 'cadastro', component: CadastroComponent , 
              canActivate: [AuthGuardService]},
  { path: 'cadastro/:id', component: CadastroComponent , 
              canActivate: [AuthGuardService]},
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes)
