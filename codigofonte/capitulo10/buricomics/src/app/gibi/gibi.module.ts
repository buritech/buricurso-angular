import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from "angular-font-awesome";

import { GibiComponent } from './gibi.component';
import { GibiListaComponent } from '../gibilista/gibilista.component';
import { routing } from '../app.routes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Arquivo: src/app/gibi/gibi.module.ts
//Código omitido acima
import { FiltroPorResumo } from './gibi.pipes';

@NgModule({
  imports: [CommonModule, NgbModule.forRoot(), 
      AngularFontAwesomeModule, routing, ],
  declarations: [GibiComponent, GibiListaComponent, 
                 FiltroPorResumo],
  exports:      [GibiComponent, GibiListaComponent, 
                 FiltroPorResumo],
  providers: []
})
export class GibiModule { }
