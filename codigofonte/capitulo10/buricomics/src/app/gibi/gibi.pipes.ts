// Arquivo: src/app/gibi/gibi.pipes.ts
import { Pipe, PipeTransform } from "@angular/core";
import { Gibi } from "../model";

@Pipe({
  name: 'filtroPorResumo'
})
export class FiltroPorResumo implements PipeTransform {
  transform(gibizada: Gibi[], consulta: string) {
    if (consulta) {
      consulta = consulta.toLowerCase().trim();
      return gibizada
        .filter(gibi => gibi.resumo.toLowerCase()
          .includes(consulta));
    }
    return gibizada
  }
}   