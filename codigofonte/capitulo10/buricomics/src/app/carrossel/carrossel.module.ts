import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CarrosselComponent } from './carrossel.component';

@NgModule({
  imports: [ CommonModule, NgbModule.forRoot()],
  declarations: [ CarrosselComponent ],
  exports: [ CarrosselComponent ]
})
export class CarrosselModule { }
