// Arquivo: src/app/model.ts
export class Usuario{
    _id: string
    login: string
    senha: string
    token: string
}
// Código omitido abaixo

export class Gibi {
    _id:string
    titulo: string
    url: string
    resumo: string
    preco: number
    constructor(){
        this.url = "assets/img/no_image.jpg"
    }
}

export interface ExclusaoListener{
    exclusaoSucesso?(gibi:Gibi): void;
}
