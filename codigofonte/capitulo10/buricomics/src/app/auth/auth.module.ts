import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Usuario } from '../model';
import { JwtHelperService } from '@auth0/angular-jwt';

export function jwtHelperServiceFactory() {
  return new JwtHelperService({
    globalHeaders: [{'Content-Type':'application/json'}],
  });
}
//Arquivo: src/app/auth/auth.module.ts
//Código acima omitido
@NgModule({
  imports: [ CommonModule ],
  declarations: [],
  providers: [
    {
      provide: JwtHelperService,
      useFactory: jwtHelperServiceFactory,
    }
  ]
})
export class AuthModule { }
