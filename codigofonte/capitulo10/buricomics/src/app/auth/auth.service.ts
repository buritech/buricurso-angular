//Arquivo: src/app/auth/auth.service.ts
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Usuario } from '../model';

@Injectable()
export class AuthService {
  private _usuario: Usuario

  constructor(private _jwtHelper: JwtHelperService) { }

  private get usuario() {
    if (!this._usuario) {
      this._usuario = JSON.parse(
        localStorage.getItem('usuarioLogado'))
    }
    return this._usuario
  }

  public isAuthenticated(): boolean {
    if (this.usuario) {
      console.log('[AuthService] token==' + this.usuario.token)
      return !this._jwtHelper.isTokenExpired(this.token)
    }
    return false
  }

  public get token() {
    if (this.usuario) {
      return this.usuario.token
    }
    return null
  }
}