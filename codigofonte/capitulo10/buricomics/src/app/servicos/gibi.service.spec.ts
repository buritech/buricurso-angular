import { TestBed, inject } from '@angular/core/testing';

import { GibiService } from './gibi.service';

describe('GibiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GibiService]
    });
  });

  it('should be created', inject([GibiService], (service: GibiService) => {
    expect(service).toBeTruthy();
  }));
});
