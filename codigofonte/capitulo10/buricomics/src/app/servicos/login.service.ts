//Arquivo: src/app/servicos/login.service.ts
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { Usuario } from '../model';

@Injectable()
export class LoginService {
  private _url: string = "http://localhost:3000/bc"
  private _httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private _httpClient: HttpClient) { }

  autenticar(usuario: Usuario): Observable<Usuario> {
      return this._httpClient
        .post<Usuario>(this._url + "/autenticar", 
                        usuario, this._httpOptions);
  }
}