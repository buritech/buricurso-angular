import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoasVindasComponent } from './boasvindas.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BoasVindasComponent],
  exports: [BoasVindasComponent]
})
export class BoasVindasModule { }
