import { Component, OnInit, Input } from '@angular/core';
import { Gibi } from '../model';

@Component({
  selector: 'gibi',
  templateUrl: './gibi.component.html',
  styleUrls: ['./gibi.component.css']
})
export class GibiComponent implements OnInit {
  @Input() gibi:Gibi
  constructor() { }
  ngOnInit() { }
}
