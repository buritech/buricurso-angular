var db = require('../../config/database');
var jwt = require('jsonwebtoken');
var api = {}
const secret = "_buricomics@server"

api.efetuarLogin = function (req, res) {
    db.findOne({
        login: req.body.login, // admin
        senha: req.body.senha  // admin
    }, function (err, usuario) {
        if (err) {
            return console.log(err);
        }
        if (usuario) {
            console.log('Usuario encontrado: ' + usuario._id);
            // valor em segundo, aqui temos um total de 24 horas
            var token = jwt.sign({ login: usuario.login }, secret, {
                expiresIn: 86400 
            });
            // adicionando token à resposta
            //res.set('x-access-token', token);
            //res.end(); // enviando a resposta
            usuario.token = token
            console.log('Autenticado: token adicionado à resposta: '+token);
            res.json(usuario);
        } else {
            console.log('Login/senha inválidos');
            res.sendStatus(401);
        }
    });
};

api.verificaToken = function (req, res, next) {
    var token = req.headers['x-access-token']; // busca o token no header da requisição

    if (token) {
        console.log('Token recebido, decodificando');
        jwt.verify(token, secret, function (err, decoded) {
            if (err) {
                console.log('Token rejeitado: '+token);
                return res.sendStatus(401);
            } else {
                console.log('Token aceito')
                // guardou o valor decodificado do token na requisição. No caso, o login do usuário.
                req.usuario = decoded;
                next();
            }
        });
    } else {
        console.log('Nenhum token enviado');
        return res.sendStatus(401);
    }
};

api.adiciona = function (req, res) {
    var gibi = req.body;
    delete gibi._id;
    db.insert(gibi, function (err, newDoc) {
        if (err)
            return console.log(err);
        console.log('Gibi cadastrado com sucesso: ' + newDoc._id);
        res.json(newDoc._id);
    });
};

api.busca = function (req, res) {
    db.findOne({ _id: req.params.gibiId }, function (err, doc) {
        if (err)
            return console.log(err);
        console.log('Gibi encontrado: ' + doc._id);
        res.json(doc);
    });
};

api.atualiza = function (req, res) {
    console.log('Parâmetro recebido:' + req.params.gibiId);
    db.update({ _id: req.params.gibiId }, req.body, function (err, numReplaced) {
        if (err) return console.log(err);
        if (numReplaced) res.status(200).end();
        res.status(500).end();
        console.log('Gibi atualizado com sucesso: ' + req.body._id);
        res.status(200).end();
    });
};

api.lista = function (req, res) {
    db.find({login : { $exists: false }}).sort({ titulo: 1 }).exec(function (err, doc) {
        if (err)
            return console.log(err);
        console.log('GET - /bc/gibis')
        res.json(doc);
    });
};

api.listaPorEditora = function (req, res) {
    var editoraId = parseInt(req.params.editoraId);
    db.find({ editora: editoraId }, function (err, doc) {
        if (err)
            return console.log(err);
        res.json(doc);
    });

};

api.remove = function (req, res) {

    db.remove({ _id: req.params.gibiId }, {}, function (err, numRemoved) {
        if (err)
            return console.log(err);
        console.log('Gibi excluído com sucesso ' + req.params.gibiId);
        if (numRemoved)
            res.status(200).end();
        res.status(500).end();
    });
};

api.listaEditoras = function (req, res) {

    res.json([
        {
            _id: 1,
            nome: 'Marvel'
        },
        {
            _id: 2,
            nome: 'DC',
        },
        {
            _id: 3,
            nome: 'Globo'
        }
    ]);

};


module.exports = api;