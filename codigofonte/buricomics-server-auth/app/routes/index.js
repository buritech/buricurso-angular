var api = require('../api'),
    path = require('path');

module.exports = function (app) {

    app.route('/bc/autenticar')
        .post(api.efetuarLogin);

    app.use('/*', api.verificaToken);

    app.route('/bc/gibis')
        .post(api.adiciona)
        .get(api.lista);

    app.route('/bc/gibis/:gibiId')
        .delete(api.remove)
        .get(api.busca)
        .put(api.atualiza);

    app.get('/bc/editoras', api.listaEditoras)
    app.get('/bc/gibis/editora/:editoraId', api.listaPorEditora);

    app.all('/*', function (req, res) {
        res.sendFile(path.join(app.get('clientPath'), 'index.html'));
    });
};